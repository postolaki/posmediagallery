# POSMediaGallery

## Example

To run the example project, clone the repo, and run `pod install` from the Demo directory first.

## Requirements
- [x] Xcode 10
- [x] Swift 5
- [x] iOS 10 or higher

## Installation

POSMediaGallery is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

``` ruby
pod 'POSMediaGallery'
```

``` swift
import POSMediaGallery

let image = UIImage(named: "image")!
let imageUrl = URL(string: "url")!
        
let videoUrl = URL(string: "url")!
let videoItem = VideoItemConfig(url: videoUrl,
                                sliderMinimumTrackTintColor: .red,
                                sliderMaximumTrackTintColor: .white)
        
let mediaItems: [MediaGalleryViewController.MediaType] = [.image(image),
                                                          .imageUrl(imageUrl),
                                                          .videoConfig(videoItem),
                                                          .custom(customView)]
let mediaGalery = MediaGalleryViewController(mediaItems: mediaItems, selectedIndex: 0)
present(mediaGalery, animated: true)
```

### Customization

``` swift
public protocol MediaGalleryDelegate: class {
    func willDisplayItem(_ item: MediaGalleryViewController.MediaType)
    func didEndDisplayingItem(_ item: MediaGalleryViewController.MediaType)
    func mediaGalleryWillDismiss()
    func mediaGalleryDidDismiss()
}
    
// media types
public enum MediaType {
    case imageUrl(URL)
    case image(UIImage)
    case videoConfig(VideoItemConfig)
    case custom(UIView)
}

// init
public init(mediaItems: [MediaType],
            selectedIndex: Int = 0,
            closeButtonConfig: CloseButtonConfig? = nil,
            imageItemConfig: ImageItemConfig? = nil)
    
// delegate
public weak var delegate: MediaGalleryDelegate?
    
// close button settings
public struct CloseButtonConfig {
    public var showCloseButton: Bool
    public var frame: CGRect?
    public var backgroundColor: UIColor?
    public var image: UIImage?
    
    public init(showCloseButton: Bool = true,
                frame: CGRect? = nil,
                backgroundColor: UIColor? = nil,
                image: UIImage? = nil)
}
    
// image settings
public struct ImageItemConfig {
    public let contentMode: UIImageView.ContentMode
    public let placeholder: UIImage?
    public let backgroundColor: UIColor?
    
    public init(contentMode: UIImageView.ContentMode = .scaleAspectFit,
                placeholder: UIImage? = nil,
                backgroundColor: UIColor? = nil)
}
    
// video settings
public struct VideoItemConfig {
    public let url: URL
    public var videoGravity: AVLayerVideoGravity
    public var showControls: Bool
    public var playImage: UIImage?
    public var pauseImage: UIImage?
    public var sliderMinimumValueImage: UIImage?
    public var sliderMaximumValueImage: UIImage?
    public var sliderMinimumTrackTintColor: UIColor?
    public var sliderMaximumTrackTintColor: UIColor?
    public var sliderThumbTintColor: UIColor?
    
    public init(url: URL,
                videoGravity: AVLayerVideoGravity = .resizeAspect,
                showControls: Bool = true,
                playImage: UIImage? = nil,
                pauseImage: UIImage? = nil,
                sliderMinimumValueImage: UIImage? = nil,
                sliderMaximumValueImage: UIImage? = nil,
                sliderMinimumTrackTintColor: UIColor? = nil,
                sliderMaximumTrackTintColor: UIColor? = nil,
                sliderThumbTintColor: UIColor? = nil)
}
```

## Author

ivan.postolaki@gmail.com

## License

POSMediaGallery is available under the MIT license.
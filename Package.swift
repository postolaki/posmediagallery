// swift-tools-version:5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(name: "POSMediaGallery",
                      platforms: [.iOS(.v12)],
                      products: [.library(name: "POSMediaGallery",
                                          targets: ["POSMediaGallery"])],
                      dependencies: [
                        .package(url: "https://github.com/onevcat/Kingfisher.git", .upToNextMajor(from: "7.4.0"))
                      ],
                      targets: [.target(name: "POSMediaGallery",
                                        dependencies: ["Kingfisher"],
                                        path: "MediaGallery",
                                        exclude: ["Info.plist", "MediaGallery.h"])
                      ],
                      swiftLanguageVersions: [.v5])

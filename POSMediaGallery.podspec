Pod::Spec.new do |s|
  s.platform = :ios
  s.ios.deployment_target = '12.0'
  s.name = "POSMediaGallery"
  s.summary = "POSMediaGallery"
  s.requires_arc = true

  s.version = "1.1.2"
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.author  = { "Ivan" => "ivan.postolaki@gmail.com" }
  s.homepage = "https://bitbucket.org/postolaki/posmediagallery"
  s.source = { :git => "https://postolaki@bitbucket.org/postolaki/posmediagallery.git", :tag => s.version.to_s }

  s.swift_version = "5"
  s.frameworks = "Foundation", "UIKit"

  s.source_files = "MediaGallery/**/*.{swift}"
  s.resources = ['MediaGallery/**/*.xib', 'MediaGallery/Resources.xcassets']

  s.dependency "Kingfisher"
end

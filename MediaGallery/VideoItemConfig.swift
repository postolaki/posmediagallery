import Foundation
import UIKit
import AVKit

public struct VideoItemConfig {
    public let url: URL
    public var videoGravity: AVLayerVideoGravity
    public var showControls: Bool
    public var playImage: UIImage?
    public var pauseImage: UIImage?
    public var sliderMinimumValueImage: UIImage?
    public var sliderMaximumValueImage: UIImage?
    public var sliderMinimumTrackTintColor: UIColor?
    public var sliderMaximumTrackTintColor: UIColor?
    public var sliderThumbTintColor: UIColor?
    
    public init(url: URL,
                videoGravity: AVLayerVideoGravity = .resizeAspect,
                showControls: Bool = true,
                playImage: UIImage? = nil,
                pauseImage: UIImage? = nil,
                sliderMinimumValueImage: UIImage? = nil,
                sliderMaximumValueImage: UIImage? = nil,
                sliderMinimumTrackTintColor: UIColor? = nil,
                sliderMaximumTrackTintColor: UIColor? = nil,
                sliderThumbTintColor: UIColor? = nil) {
        self.url = url
        self.videoGravity = videoGravity
        self.showControls = showControls
        self.playImage = playImage
        self.pauseImage = pauseImage
        self.sliderMinimumValueImage = sliderMinimumValueImage
        self.sliderMaximumValueImage = sliderMaximumValueImage
        self.sliderMinimumTrackTintColor = sliderMinimumTrackTintColor
        self.sliderMaximumTrackTintColor = sliderMaximumTrackTintColor
        self.sliderThumbTintColor = sliderThumbTintColor
    }
}

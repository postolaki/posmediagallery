import Foundation
import UIKit

public struct ImageItemConfig {
    public let contentMode: UIImageView.ContentMode
    public let placeholder: UIImage?
    public let backgroundColor: UIColor?
    
    public init(contentMode: UIImageView.ContentMode = .scaleAspectFit,
                placeholder: UIImage? = nil,
                backgroundColor: UIColor? = nil) {
        self.contentMode = contentMode
        self.placeholder = placeholder
        self.backgroundColor = backgroundColor
    }
}

import UIKit

final class CustomFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        scrollDirection = .horizontal
        minimumLineSpacing = 0
        minimumInteritemSpacing = 0
        sectionInset = .zero
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        guard let cell = collectionView?.visibleCells.first,
            let indexPath = collectionView?.indexPath(for: cell),
            let width = collectionView?.frame.width else {
                return .zero
        }
        
        return CGPoint(x: width * CGFloat(indexPath.item), y: 0)
    }
}

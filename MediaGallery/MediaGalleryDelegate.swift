import Foundation

public protocol MediaGalleryDelegate: AnyObject {
    func willDisplayItem(_ item: MediaGalleryViewController.MediaType)
    func didEndDisplayingItem(_ item: MediaGalleryViewController.MediaType)
    func mediaGalleryWillDismiss()
    func mediaGalleryDidDismiss()
}

// make it optional
public extension MediaGalleryDelegate {
    func willDisplayItem(_ item: MediaGalleryViewController.MediaType){}
    func didEndDisplayingItem(_ item: MediaGalleryViewController.MediaType){}
    func mediaGalleryWillDismiss(){}
    func mediaGalleryDidDismiss(){}
}

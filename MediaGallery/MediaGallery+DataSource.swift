import Foundation
import UIKit

extension MediaGalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaItems.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let mediaItem = mediaItems[indexPath.row]
        switch mediaItem {
        case .imageUrl(let url):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier(MediaGalleryImageCVC.self),
                                                          for: indexPath) as! MediaGalleryImageCVC
            cell.setup(with: url, config: imageItemConfig)
            return cell
        case .image(let image):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier(MediaGalleryImageCVC.self),
                                                          for: indexPath) as! MediaGalleryImageCVC
            cell.setup(with: image, config: imageItemConfig)
            return cell
        case .videoConfig(let config):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier(MediaGalleryVideoCVC.self),
                                                          for: indexPath) as! MediaGalleryVideoCVC
            cell.setup(with: config)
            return cell
        case .custom(let view):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier(MediaGalleryCustomViewCVC.self),
                                                          for: indexPath) as! MediaGalleryCustomViewCVC
            cell.setup(with: view)
            return cell
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? MediaGalleryVideoCVC {
            cell.play()
        }
        let mediaItem = mediaItems[indexPath.row]
        delegate?.willDisplayItem(mediaItem)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? MediaGalleryVideoCVC {
            cell.pause()
        }
        let mediaItem = mediaItems[indexPath.row]
        delegate?.didEndDisplayingItem(mediaItem)
    }
}

import Foundation
import UIKit

public struct CloseButtonConfig {
    public var showCloseButton: Bool
    public var frame: CGRect?
    public var backgroundColor: UIColor?
    public var image: UIImage?
    
    public init(showCloseButton: Bool = true,
                frame: CGRect? = nil,
                backgroundColor: UIColor? = nil,
                image: UIImage? = nil) {
        self.showCloseButton = showCloseButton
        self.frame = frame
        self.backgroundColor = backgroundColor
        self.image = image
    }
}

import UIKit

class MediaGalleryCustomViewCVC: UICollectionViewCell {

    func setup(with view: UIView) {
        addSubview(view)
        view.pinTo(view: self)
    }
}

import Foundation
import UIKit
import AVKit

protocol PlayerViewDelegate: AnyObject {
    func playingStateChanged(isPlaying: Bool)
    func bufferingStateChanged(isBuffering: Bool)
    func didReceiveDuration(_ duration: Float)
}

final class PlayerView: UIView {
    
    weak var delegate: PlayerViewDelegate?
    
    var url: URL! {
        didSet {
            setupPlayer()
        }
    }
    
    var isPlaying: Bool = false {
        didSet {
            delegate?.playingStateChanged(isPlaying: isPlaying)
        }
    }
    
    var videoGravity: AVLayerVideoGravity = .resizeAspect
    
    private let keyPaths = ["playbackBufferEmpty", "playbackLikelyToKeepUp", "playbackBufferFull"]
    
    private lazy var playerLayer: AVPlayerLayer = {
        let playerLayer = AVPlayerLayer()
        layer.addSublayer(playerLayer)
        
        return playerLayer
    }()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        playerLayer.frame = bounds
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
    }
    
    private func setupPlayer() {
        let asset = AVAsset(url: url)
        let avplayerItem = AVPlayerItem(asset: asset)
        playerLayer.player = AVPlayer(playerItem: avplayerItem)
        playerLayer.frame = bounds
        playerLayer.videoGravity = videoGravity
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: playerLayer.player?.currentItem)
        playerLayer.player?.addObserver(self,
                                        forKeyPath: "timeControlStatus",
                                        options: .new,
                                        context: nil)
        keyPaths.forEach {
            avplayerItem.addObserver(self,
                                     forKeyPath: $0,
                                     options: .new,
                                     context: nil)
        }

        DispatchQueue.global(qos: .background).async {
            let durationTime = CMTimeGetSeconds(asset.duration)
            DispatchQueue.main.async { [weak self] in
                self?.delegate?.didReceiveDuration(Float(durationTime))
            }
        }
    }
    
    func play() {
        playerLayer.player?.play()
    }
    
    func pause() {
        playerLayer.player?.pause()
    }
    
    var currentTime: Float {
        guard let time = playerLayer.player?.currentTime() else { return 0 }
        return Float(CMTimeGetSeconds(time))
    }
    
    func seek(to value: Double) {
        if let timescale = playerLayer.player?.currentItem?.asset.duration.timescale {
            let time = CMTimeMakeWithSeconds(Float64(value), preferredTimescale: timescale)
            playerLayer.player?.seek(to: time)
        }
    }
    
    @objc private func playerItemDidReachEnd(_ notification: Notification) {
        if let item = notification.object as? AVPlayerItem {
            item.seek(to: .zero, completionHandler: nil)
            playerLayer.player?.play()
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath {
        case "timeControlStatus":
            isPlaying = playerLayer.player?.timeControlStatus == .playing
        case "playbackBufferEmpty":
            delegate?.bufferingStateChanged(isBuffering: true)
        case "playbackLikelyToKeepUp", "playbackBufferFull":
            delegate?.bufferingStateChanged(isBuffering: false)
        default:
            break
        }
    }
    
    deinit {
        playerLayer.player?.removeObserver(self, forKeyPath: "timeControlStatus")
        keyPaths.forEach {
            playerLayer.player?.currentItem?.removeObserver(self, forKeyPath: $0)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
}

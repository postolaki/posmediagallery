import UIKit
import Kingfisher

class MediaGalleryImageCVC: UICollectionViewCell, UIScrollViewDelegate {
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    func setup(with url: URL, config: ImageItemConfig) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        imageView.kf.setImage(with: url,
                              placeholder: config.placeholder,
                              options: [.transition(.fade(0.5))]) { [weak self] result in
            self?.activityIndicator.stopAnimating()
        }

        imageView.contentMode = config.contentMode
        imageView.backgroundColor = config.backgroundColor
        setupScrollView()
    }
    
    func setup(with image: UIImage, config: ImageItemConfig) {
        imageView.contentMode = config.contentMode
        imageView.backgroundColor = config.backgroundColor
        imageView.image = image
        setupScrollView()
    }
    
    private func setupScrollView() {
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 4.0
        scrollView.delegate = self
    }
    
    // MARK: - UIScrollViewDelegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        imageView.clipsToBounds = false
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.scrollView.zoomScale = 1.0
        }) { [weak self] _ in
            self?.imageView.clipsToBounds = true
        }
    }
}

import UIKit
import AVKit

class MediaGalleryVideoCVC: UICollectionViewCell, PlayerViewDelegate {
    
    @IBOutlet private weak var playerView: PlayerView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var slider: UISlider!
    @IBOutlet private weak var controlsStackView: UIStackView!
    
    private var config: VideoItemConfig!
    private lazy var timer: DispatchSourceTimer = {
        let identifier = playerView.url.absoluteString
        let timer = DispatchSource.makeTimerSource(flags: [], queue:  DispatchQueue(label: "\(identifier)"))
        return timer
    }()
    
    func setup(with config: VideoItemConfig) {
        self.config = config
        playerView.delegate = self
        playerView.videoGravity = config.videoGravity
        playerView.url = config.url
        controlsStackView.isHidden = !config.showControls
        slider.minimumValueImage = config.sliderMinimumValueImage
        slider.maximumValueImage = config.sliderMaximumValueImage
        slider.minimumTrackTintColor = config.sliderMinimumTrackTintColor
        slider.maximumTrackTintColor = config.sliderMaximumTrackTintColor
        slider.thumbTintColor = config.sliderThumbTintColor
    }
    
    func play() {
        playerView.play()
    }
    
    func pause() {
        playerView.pause()
    }
    
    @IBAction private func didPressPlayButton(_ sender: UIButton) {
        playerView.isPlaying ? pause() : play()
    }
    
    @IBAction private func sliderValueChanged(_ sender: UISlider) {
        if !sender.isTracking {
            playerView.seek(to: Double(sender.value))
        }
    }
    
    func playingStateChanged(isPlaying: Bool) {
        let pauseImage = config.pauseImage ?? UIImage(named: "pause", in: Bundle(for: classForCoder), compatibleWith: nil)
        let playImage = config.playImage ?? UIImage(named: "play", in: Bundle(for: classForCoder), compatibleWith: nil)
        let image = isPlaying ? pauseImage : playImage
        UIView.transition(with: playButton,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { [weak self] in
                            self?.playButton.setImage(image, for: .normal)
        })
    }
    
    func bufferingStateChanged(isBuffering: Bool) {
        isBuffering ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }
    
    func didReceiveDuration(_ duration: Float) {
        slider.minimumValue = 0
        slider.maximumValue = duration
        
        timer.schedule(deadline: .now(), repeating: .milliseconds(100))
        timer.setEventHandler { [weak self] in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                if !self.slider.isTracking {
                    self.slider.value = self.playerView.currentTime
                }
            }
        }
        timer.activate()
    }
    
    deinit {
        timer.cancel()
    }
}

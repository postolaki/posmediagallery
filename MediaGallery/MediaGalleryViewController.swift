import UIKit

open class MediaGalleryViewController: UIViewController {
    
    // MARK: - public accesors
    
    public enum MediaType {
        case imageUrl(URL)
        case image(UIImage)
        case videoConfig(VideoItemConfig)
        case custom(UIView)
    }
    
    public weak var delegate: MediaGalleryDelegate?
    
    public let closeButtonConfig: CloseButtonConfig?
    public let mediaItems: [MediaGalleryViewController.MediaType]
    
    // MARK: - private vars

    private var initialTouchPoint = CGPoint.zero
    private let selectedIndex: Int
    let imageItemConfig: ImageItemConfig
    
    private lazy var collectionViewFlowLayout: CustomFlowLayout = {
        let layout = CustomFlowLayout()
        layout.itemSize = view.frame.size
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: collectionViewFlowLayout)
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = .black
        view.addSubview(collectionView)
        collectionView.pinTo(view: view)
        return collectionView
    }()
    
    private lazy var blurView: UIView = {
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return blurEffectView
    }()
    
    open lazy var closeButton: UIButton = {
        var startPonint = CGPoint(x: 16, y: 8)
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top ?? 0
            startPonint.y += topPadding
        }
        var frame = CGRect(origin: startPonint, size: CGSize(width: 40, height: 40))
        
        if let closeButtonConfig = closeButtonConfig {
            if let _frame = closeButtonConfig.frame {
                frame = _frame
            }
        }
        
        let button = UIButton(frame: frame)
        #if SWIFT_PACKAGE
            let bundle = Bundle.module
        #else
            let bundle = Bundle(for: classForCoder)
        #endif
        let image = UIImage(named: "close", in: bundle, compatibleWith: nil)
        button.setImage(closeButtonConfig?.image ?? image, for: .normal)
        button.backgroundColor = closeButtonConfig?.backgroundColor
        button.addTarget(self, action: #selector(didPressCloseButton), for: .touchUpInside)
        
        return  button
    }()
    
    public required init?(coder aDecoder: NSCoder) {
        mediaItems = []
        selectedIndex = 0
        closeButtonConfig = nil
        imageItemConfig = ImageItemConfig()
        super.init(coder: aDecoder)
        modalPresentationStyle = .overCurrentContext
    }
    
    public init(mediaItems: [MediaType],
                selectedIndex: Int = 0,
                closeButtonConfig: CloseButtonConfig? = nil,
                imageItemConfig: ImageItemConfig? = nil) {
        self.mediaItems = mediaItems
        self.selectedIndex = selectedIndex
        self.closeButtonConfig = closeButtonConfig
        self.imageItemConfig = imageItemConfig ?? ImageItemConfig()
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overCurrentContext
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        addPanGestureRecognizer()
        collectionView.contentInsetAdjustmentBehavior = .never
        setupCollectionView()
        presentingViewController?.view.addSubview(blurView)
        view.addSubview(closeButton)
    }
    
    override open var shouldAutorotate: Bool {
        return true
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateCollectionViewLayout(size: size)
    }
    
    func updateCollectionViewLayout(size: CGSize) {
        let layout = collectionViewFlowLayout
        layout.itemSize = size
        collectionView.collectionViewLayout = layout
    }
    
    @objc open func didPressCloseButton() {
        dimissController()
    }
    
    open func dimissController() {
        delegate?.mediaGalleryWillDismiss()
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.blurView.alpha = 0
        }
        dismiss(animated: true) { [weak self] in
            self?.delegate?.mediaGalleryDidDismiss()
            self?.blurView.removeFromSuperview()
        }
    }
    
    private func setupCollectionView() {
        registerCells()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        
        guard selectedIndex < mediaItems.count else { return }
        let indexPath = IndexPath(item: selectedIndex, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
    }
    
    private func registerCells() {
        collectionView.register(nib(MediaGalleryImageCVC.self),
                                forCellWithReuseIdentifier: identifier(MediaGalleryImageCVC.self))
        collectionView.register(nib(MediaGalleryVideoCVC.self),
                                forCellWithReuseIdentifier: identifier(MediaGalleryVideoCVC.self))
        collectionView.register(nib(MediaGalleryCustomViewCVC.self),
                                forCellWithReuseIdentifier: identifier(MediaGalleryCustomViewCVC.self))
    }
    
    private func nib(_ className: UICollectionViewCell.Type) ->  UINib? {
        let identifier = String(describing: className)
        #if SWIFT_PACKAGE
            let bundle = Bundle.module
        #else
            let bundle = Bundle(for: classForCoder)
        #endif
        let nib = UINib(nibName: identifier, bundle: bundle)
        return nib
    }
    
    func identifier(_ className: UICollectionViewCell.Type) -> String {
        return String(describing: className)
    }
    
    private func addPanGestureRecognizer() {
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        view.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc private func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: view.window)
        
        if sender.state == .began {
            initialTouchPoint = touchPoint
        } else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: view.frame.size.width, height: view.frame.size.height)
                updateBlur(alpha: CGFloat(fabsf(Float(view.frame.origin.y / view.bounds.height - 1))))
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 200 {
                delegate?.mediaGalleryWillDismiss()
                dismiss(animated: true) { [weak self] in
                    self?.delegate?.mediaGalleryDidDismiss()
                    self?.blurView.removeFromSuperview()
                }
            } else {
                UIView.animate(withDuration: 0.3, animations: { [weak self] in
                    guard let _self = self else { return }
                    _self.view.frame = CGRect(x: 0, y: 0, width: _self.view.frame.size.width, height: _self.view.frame.size.height)
                }) { [weak self] _ in
                    self?.updateBlur(alpha: 1)
                }
            }
        }
    }
    
    private func updateBlur(alpha: CGFloat) {
        blurView.alpha = alpha
    }
}

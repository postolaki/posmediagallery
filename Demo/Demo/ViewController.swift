import UIKit
import POSMediaGallery

class ViewController: UIViewController, MediaGalleryDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction private func didPressShowGallery(_ sender: UIButton) {
        let image = UIImage(named: "ninja")!
        let someImageInGooleSearch = "https://s3-us-west-2.amazonaws.com/uw-s3-cdn/wp-content/uploads/sites/6/2017/11/04133712/waterfall.jpg"
        let imageUrl = URL(string: someImageInGooleSearch)!
        
        let videoUrl = URL(string: "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")!
        let videoItem = VideoItemConfig(url: videoUrl,
                                        sliderMinimumTrackTintColor: .red,
                                        sliderMaximumTrackTintColor: .white)
        
        let mediaItems: [MediaGalleryViewController.MediaType] = [.image(image),
                                                                  .imageUrl(imageUrl),
                                                                  .videoConfig(videoItem),
                                                                  .custom(customView)]
        let mediaGalery = MediaGalleryViewController(mediaItems: mediaItems, selectedIndex: 0)
        mediaGalery.delegate = self
        present(mediaGalery, animated: true)
    }
    
    var customView: UIView {
        let view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = .red
        return view
    }
    
    func willDisplayItem(_ item: MediaGalleryViewController.MediaType) {
        print(item, "willDisplayItem")
    }
    
    func didEndDisplayingItem(_ item: MediaGalleryViewController.MediaType) {
        print(item, "didEndDisplayingItem")
    }
    
    func mediaGalleryDidDismiss() {
        print(#function)
    }
    
    func mediaGalleryWillDismiss() {
        print(#function)
    }
}
